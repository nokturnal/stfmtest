; #################################################################################################
; # Atari ST/e/TT/F030 OPL2 / OPL3 Centronics / Cartridge output test
; # Centronics/LPT port replay: Pawel Goralski / [nokturnal]
; # ROM port replay, sample song data, os helper macros: Daniel Illgen / [TSCC]
; # (c) 2019-2022 Pawel Goralski, Daniel Illgen
; #################################################################################################

LOOP_REPLAY				equ		1		; play tune in loop

; ##################################################################################################
; includes
	include "inc/tosmacros.inc"
	include "inc/common.inc"
	include "inc/opl.inc"
	
; ##################################################################################################
; code section
	TEXT

	if	USE_CENTRONICS_OUTPUT
		echo    'Building with output via Printer / Centronics port'
main:
	tos_init
	
	jsr		super_on
	ori.b 	#BIT0,LPT_DATA_DIRECTION.w		; output, bit #0 centronics busy

	StrobeHigh								; OPL2/3 /WR high

	if	ENABLE_OPL3	
	echo    'Building OPL3 test variant'
	SelectInLow
	else
	echo    'Building OPL2 test variant'
	SelectInLow
	endif

	tos_cconws appinfo	

	tos_cconws playingtestsongmsg
		
mainloop:
	;a0 psg adress
	;a1 acia data
	;a4 adlib data

	lea		adlibdata,a4
	move.w	#200,d2
	
	lea.l  ACIA_DATA,a1

.readloop:
	cmpi.b  #SC_SPACE,(a1)
	beq    .quitRequest

	move.b	(a4)+,d3		; read register select
	beq		.adlibwait
	move.b	(a4)+,d4        ; read data

	bsr.w 	oplLptWrite
	bra.s	.readloop
.adlibdone:
	rts

.adlibwait:
	;move.w	#$fff,$ffff8240.w
	move.l	$4ba.w,d3
.wt200:	
    cmp.l	$4ba.w,d3
	beq.s	.wt200
	sub.w	#70,d2
	bpl.s	.adlibwait
	add.w	#200,d2
	move.b	(a4)+,d3

	if      LOOP_REPLAY
	bmi.s	mainloop
	else	
	bmi.s	.adlibdone
	endif

	;move.w	#0,$ffff8240.w
	bra.s	.readloop

.quitRequest:
	
	jsr oplReset
	; reset to initial state
	; /SelectIn low, /Busy high, /Strobe high 
	PsgWrite	YM_PORT_B,BITS_OFF
	SelectInLow
	StrobeHigh
	DataWriteMode
	
	jsr	super_off

	tos_cconws byemsg	
	tos_cconin 	
			
	tos_halt

	else
		echo    'Building with output via Cartridge port'

	tos_init
	jsr super_on

mainloop:
	;a0 psg adress
	;a1 acia data
	;a4 adlib data

	lea		adlibdata,a4
	move.w	#200,d2
	
	lea.l  ACIA_DATA,a1

.readloop:
	cmpi.b  #SC_SPACE,(a1)
	beq    .quitRequest

	move.b	(a4)+,d3		; read register select
	beq		.adlibwait
	move.b	(a4)+,d4        ; read data

	bsr.w 	oplLptWrite
	bra.s	.readloop
.adlibdone:
	rts

.adlibwait:
	;move.w	#$fff,$ffff8240.w
	move.l	$4ba.w,d3
.wt200:	
    cmp.l	$4ba.w,d3
	beq.s	.wt200
	sub.w	#70,d2
	bpl.s	.adlibwait
	add.w	#200,d2
	move.b	(a4)+,d3

	if      LOOP_REPLAY
	bmi.s	mainloop
	else	
	bmi.s	.adlibdone
	endif

	;move.w	#0,$ffff8240.w
	bra.s	.readloop

.quitRequest:

	jsr	super_off

	tos_cconws byemsg	
	tos_cconin 	
			
	tos_halt

	endif

; oplLptWrite
; ------------------------------------------------------------------------------------------------
; d3 = opl2/3 register access mode (0 - address write mode, 1 - data write mode)
; d4 = opl2 / 3 data

; on enter strobe is set to high (\BUSY high),
; clean up d3,d4
; if d3 == 0 then register select, 
; if d3 == 0 and d4 == 0 , then delay one 70Hz frame 
;                        , else set register select and perform lpt data write, delay 4us. 
; if d4 == 1 then data register write, set register data write, perform lpt data write, delay 24us  
; Set Strobe low (/BUSY), wait  100ns, set Strobe High (/BUSY)
; finish

oplLptWrite:
	if	USE_CENTRONICS_OUTPUT

	AddressWriteMode
	
	if ENABLE_OPL3
	; TODO: for tunes supporting OPL3 this signal will set to L/H, depending on song data
	SelectInLow
	endif

	PsgWriteReg	YM_PORT_B,d3

	StrobeLow			; /WR low
                        ; delay after /WR low
	rept OPL_DELAY_STROBE_HOLD
	nop 
	endr
	
	StrobeHigh			; OPL2/3 /WR high

	; delay after address write
	rept OPL_DELAY_ADDR_WRITE
	nop 
	endr

	DataWriteMode
	PsgWriteReg	YM_PORT_B,d4

	StrobeLow			; /WR low
						; delay after /WR low
	rept OPL_DELAY_STROBE_HOLD
	nop 
	endr

	StrobeHigh			; OPL2/3 /WR high
	
	; register data write delay
	rept OPL_DELAY_DATA_WRITE
	nop 
	endr

	else
	
	; centronics part
	; The 'tst' lines put the address lines on the cart connector - we can't write on the cart port, so we need to use reads "for writes"
	; as well as using the address lines as data lines. 
	; reading from fa0000 selects reg-select/opl2-a0=0; reading from fa0200 select regs-data/opl2-a0=1.
	; a2 is only used for timing purposes.

	move.l	#$fa0000,a0 	; reading from fa0000 selects reg-select/opl2-a0=0; 
	move.l	#$fa0200,a1		; reading from fa0200 select regs-data/opl2-a0=1.
	move.l	#$fb0000,a2		; a2 - timing
	and.w	#$ff,d3
	and.w	#$ff,d4
	add.w	d3,d3
	add.w	d4,d4
	tst.b	(a0,d3)
;	tst.b	(a0,d0.w*2)
;   tst.w (a0)=8
;   tst.w (0(a0,d0.w))=16
;   same as move

	rept OPL_DELAY_ADDR_WRITE
	nop 
	endr

;	or.w	#$0100,d1
;	tst.b	(a0,d1.w*2)
	tst.w	(a1,d4)

	rept OPL_DELAY_DATA_WRITE
	nop 
	endr
	
	endif
	rts

	include "common.asm"
	include "opl.asm"

    DATA

; ##################################################################################################    
; The raw dump is opl2 register + opl2 data. 

adlibdata:
song:
	rept	$f0  ;240
	dc.b	$10 + REPTN,0
	endr
	
	rept	$20   ;32
	dc.b	$40 + REPTN,$3f
	endr
	
	dc.b	1,$20 
	dc.b	8,0
	dc.b	$bd,0
	
	incbin	"data/song2.raw"
	
	rept	$f0  ;240
	dc.b	$10 + REPTN,0
	endr
	
	rept	$20	;32
	dc.b	$40 + REPTN,$3f
	endr

	dc.b	1,$20
	dc.b	8,0
	dc.b	$bd,0
	dc.b	0,$ff

appinfo:

	if ENABLE_OPL3

	if	USE_CENTRONICS_OUTPUT
	dc.b 	$0d,$0a,"OPL3LPT output test",$0d,$0a
	dc.b 	"=========================================",$0d,$0a
	else
	dc.b 	$0d,$0a,"OPL3 Cartridge port output test",$0d,$0a
	dc.b 	"======================================",$0d,$0a
	endif
	
	else

	if	USE_CENTRONICS_OUTPUT
	dc.b 	$0d,$0a,"OPL2LPT output test",$0d,$0a
	dc.b 	"=========================================",$0d,$0a
	else
	dc.b 	$0d,$0a,"OPL2 Cartridge port output test",$0d,$0a
	dc.b 	"======================================",$0d,$0a
	endif

	endif

	dc.b    "OPL2 Cartridge port replay, sample song data, signal rewiring ideas:",$0d,$0a,"Daniel Illgen Insane/[tscc]",$0d,$0a
	dc.b 	"Centronics port replay, OPL3 support, adapters schematics:",$0d,$0a,"Pawel Goralski Saulot/[nokturnal]",$0d,$0a
	dc.b 	"(c)2019-2022 [nokturnal]",$0d,$0a,0

playingtestsongmsg:
	dc.b    $0d,$0a,$0d,$0a,"Now playing test tune in a loop...",$0d,$0a,"Press SPACE to quit.",$0d,$0a,0

byemsg:
	dc.b    $0d,$0a,$0d,$0a,"Bye...",$0d,$0a,0
