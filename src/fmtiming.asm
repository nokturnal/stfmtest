; #################################################################################################
; # Atari ST/e/TT/F030 OPL2 / OPL3 Centronics timing test
; # vasm os macros: Daniel Illgen / [TSCC]
; # (c) 2019-2022 Pawel Goralski / [nokturnal]
; #################################################################################################

; ##################################################################################################
; includes
	include "inc/tosmacros.inc"
	include "inc/common.inc"
	include "inc/opl.inc"

; ##################################################################################################
; code section

	TEXT
main:
	tos_init
	
	jsr	super_on

	tos_cconws appinfo
	tos_cconws timingtestmsg

	ori.b 	#BIT0,LPT_DATA_DIRECTION.w		; output, bit #0 centronics busy

	StrobeLow

.timingTestLoop:
	lea.l   ACIA_DATA,a1
	cmpi.b  #SC_SPACE,(a1)
	beq    .quitRequest
	
	StrobeHigh
	rept OPL_DELAY_STROBE_HOLD
	nop 
	endr
	StrobeLow

	StrobeHigh
	rept OPL_DELAY_DATA_WRITE
	nop 
	endr
	StrobeLow

	StrobeHigh
	rept OPL_DELAY_ADDR_WRITE
	nop 
	endr
	StrobeLow
	
	bra 	.timingTestLoop

.quitRequest:
	move.b  #0,(a1)
	tos_cconws timingtestendmsg

	; reset to initial state
	; /SelectIn low, /Busy high, /Strobe high
	lea.l  		PSG_BASE,a0 
	PsgWrite	YM_PORT_B,BITS_OFF
	SelectInLow
	StrobeHigh
	DataWriteMode
	
	jsr	super_off

	tos_cconws byemsg	
	tos_cconin 	
			
	tos_halt
	
	include "common.asm"

; ##################################################################################################    
    DATA

appinfo:
	if ENABLE_OPL3
	dc.b 	"OPL3 Centronics timing test",$0d,$0a
	else
	dc.b 	"OPL2 Centronics timing test",$0d,$0a
	endif
	
	dc.b 	"======================================",$0d,$0a
	dc.b 	"(c)2022 Pawel Goralski / [nokturnal]",$0d,$0a,0

timingtestmsg:
	dc.b    $0d,$0a,"Testing delay periods. Press SPACE to exit.",$0d,$0a,0

timingtestendmsg:
	dc.b    "Delay periods tests finished...",$0d,$0a,0

byemsg:
	dc.b    $0d,$0a,$0d,$0a,"Press any key to return to desktop...",$0d,$0a,0
