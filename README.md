# FMPLAY #
[Simple test program](https://bitbucket.org/nokturnal/stfmtest/downloads/fmplay.tos) for playing music with OPL3LPT / OPL2LPT FM synth connected via Atari printer port. OPL3LPT / OPL2LPT needs basic adapter for correct operation with 16/32 bit Atari's. Schematics are provided in KiCad file format. 

[![FMPLAY.TOS Atari TOS demo](https://img.youtube.com/vi/j3emNvrAVwA/0.jpg)](https://www.youtube.com/watch?v=j3emNvrAVwA)

# **Warning!** #
Due to hardware design of a printer port and pullups on /BUSY signal in older STs, which is 'misused' as an output in this project, connecting and using this adapter might be risky, although /BUSY line has additional resistor inserted on an adapter as a precaution. On Mega ST and newer models those pullups are weaker, so risk is much more reduced. Take it into account before using this adapter, as I cannot be responsible for any damage of your hardware. Use it at your own risk! Adapter was tested on Atari 520ST+ and Falcon 030. 

Atari ST OPL2/3 cartridges doesn't exist yet, only early prototypes, so no schematics are provided.

# Downloads:
 Atari TOS builds (OPL2/3) can be downloaded [here](https://bitbucket.org/nokturnal/stfmtest/downloads)

# OPL3LPT wiring diagram
Below are informations about adapter needed to connect Atari Falcon030 / ST computers to OPL2LPT / OPL3LPT via printer port. Without it OPL3LPT will not function properly.
Needed parts are: DB25 Male and Female ports, 330Ohm resistor(optional on anything Mega ST and up) and simple dip switch, which can be replaced by three soldered pins with a jumper (relevant only on TT and Falcon). Optional switch for Atari Mega/ST/e to enable OPL3 mode.
As on basic STs (models earlier than Atari TT) there is not enough signals and 17th pin on OPL3LPT needs to be connected to GND.

\RD signal is set to high permanently on OPL3LPT, so there is no possibility to read back from OPL2/3 chip - OPL2/3 is permanently in 'write' mode. 
Pullup resistor to /STROBE pin (1kOhm) on OPL3LPT needs to be removed if it is present. Also quality of USB cable and power source is important, when powering OPL3LPT, otherwise it will not sound properly.

![OPL3LPT schematics](img/OPL3LPTAdapter_schematics.png){width=25%}

| OPLxLPT | Direction | Centronics | Atari TT / Falcon030 | Atari ST |
| :-- | :-: | :-- | :-- | :-- |
| 1: A0 |  -> | /Strobe   CTRL-0 | 1 /STROBE | 1 /STROBE |
| 2: D0 | <-> | Data0 | 2 /DATA0	 | 2 /DATA0 |
| 3: D1 | <-> | Data1 | 3 /DATA1	 | 3 /DATA1 |
| 4: D2 | <-> | Data2 | 4 /DATA2	 | 4 /DATA2 |
| 5: D3 | <-> | Data3 | 5 /DATA3	 | 5 /DATA3 |
| 6: D4 | <-> | Data4 | 6 /DATA4	 | 6 /DATA4 |
| 7: D5 | <-> | Data5 | 7 /DATA5	 | 7 /DATA5 |
| 8: D6 | <-> | Data6 | 8 /DATA6	 | 8 /DATA6 |
| 9: D7 | <-> | Data7 | 9 /DATA7	 | 9 /DATA7 |
| 14: /RD |  -> | /Linefeed CTRL-1 | 14 Not connected| 14 Not connected |
| 16: /WR |  -> | Init      CTRL-2 | 11 /BUSY	 | (via 330 Ohm resistor)11 /BUSY |
| 17: A1 |  -> | /Select    CTRL-3 | 17 /SELIN | 17 Not connected |
| 18-25: GND |  -> | GND | 18-25 GND | 18-25 GND |

![OPL3LPT adapter visualisation](img/Opl3LptAdapter3dView.png){width=25%}

# YM3812 (OPL2) / YMF262 (OPL3) chip programming
To send commands of the OPL2/3 we need to set D0-D7 and A0 ( issue register select or data select), then strobe the /WR line (/BUSY on Atari side) - set it to 0 for 100ns and then back to 1.

After that we need to make delay: 
* If we have used Register Select (A0 = 0, 4us for OPL2, 2,3us for OPL3)
* If we have used Register Data Write (A0 = 1, ~24us for OPL2, 2,3us for OPL3).
* If we want to operate in OPL3 mode (A1 = 1), we need set /SELIN to high (supported only on Atari TT/Falcon 030).

# Atari LPT / Centronics programming 101
* On the Atari ST we only have /BUSY signal,which we configure as an output, D0-D7 data lines, as well as /STROBE. 
* Atari Falcon030/TT has additionally /ACK, which isn't useful at all as it is input only (probably can be reconfigured in same way as /BUSY signal) and /SELIN used to set OPL3LPT into OPL3 mode. Without it OPL3LPT operates in OPL2 mode. 
* Bits are numbered 0-7 and 0 is most significant, rightmost bit. 
* Data lines D0-D7 can be accessed via the PSG port B - write 15 to ym2149 / PSG ($FF8800) and output line states to $FF8802 address.
* /STROBE can be set by modifying bit #5 in ym2149 / PSG ($FF880) port A, register 14.
* /BUSY can be set via MFP ($FFFA01) bit #0, also we need to set bit #0 of data direction ($FFFA05) - 1 sets output direction mode.
* /SELIN can be set by modifying bit #3 in ym2149 / PSG ($FF880) port A, register 14.
* ym2149 / PSG can be accessed via Giaccess, but it's **slow**.

## Repository contains following folders:
* "src" folder contains test program sources.
* "reference" contains audio reference recorded from OPL2/3 device connected to cartridge port. Similar output need to be audible when using FMPLAY.TOS.
* "pcb" contains KiCAD adapter schematics.

## Build / run test programs
Requirements: Only [VASM, a portable and retargetable assembler](http://sun.hasenbraten.de/vasm/), GNU make and bash shell is required to build samples.

* Go to "src" directory.
* Type in 'make all' for OPL2 build.
* Type in 'make ENABLE_OPL3=1 all' for OPL3 build.
* If you have UIPTool, adjust CPU, TARGET_IP and TOS_DIR, type in 'make deploy' or 'make ENABLE_OPL3=1 deploy'. Otherwise just copy **FMPLAY.TOS** (other programs aren't interesting from end-user perspective).
* Launch **FMPLAY.TOS** from opl2 or opl3 folder.

# Credits
* Original OPL2 cartridge replay code, cartridge adapter idea: Daniel Illgen ( Insane/TSCC )
* OPL3 support, documentation, Atari printer port adaptation, 16/32 bit Atari adapter schematics : Paweł Góralski ( Saulot / Nokturnal )

## Special thanks to:
Arne, MPatton, Ijor, Czietz, Sqward, T0ri from Atari Forum / Atari Area

Use is prohibited for commercial purposes without written permission from authors.

If you like this project or others feel free to donate to help cover partially my development time and expenses related to hardware. Thank you! 
[![](https://www.paypalobjects.com/en_US/i/btn/btn_donate_SM.gif)](https://www.paypal.com/donate/?hosted_button_id=UTEF2ABTDNSMC)

(c)2019-2023 [Paweł Góralski](https://nokturnal.pl), [Daniel Illgen](https://insane.tscc.de)
