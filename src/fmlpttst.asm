; #################################################################################################
; # Atari ST/e/TT/F030 LPT XBIOS/no XBIOS test
; # vasm os macros: Daniel Illgen / [TSCC]
; # (c) 2022 Pawel Goralski / [nokturnal]
; #################################################################################################

; ##################################################################################################
; includes
	include "inc/tosmacros.inc"
	include "inc/common.inc"
	include "inc/opl.inc"
	
; ##################################################################################################
; code section

	TEXT
main:
	tos_init
	jsr	super_on
	ori.b 	#BIT0,LPT_DATA_DIRECTION.w		; output, bit #0 centronics busy

	tos_cconws appinfo	
	
	tos_cconws	byemsg			

mainloop:
	lea.l   ACIA_DATA,a1
	cmpi.b  #SC_SPACE,(a1)
	beq    .quitRequest

	SelectInLow
	StrobeHigh
	AddressWriteMode

	PsgWrite	YM_PORT_B,BITS_OFF
	PsgWrite	YM_PORT_B,BITS_ON
	PsgWrite	YM_PORT_B,BITS_OFF
	PsgWrite	YM_PORT_B,(BIT0)
	PsgWrite	YM_PORT_B,(BIT1)
	PsgWrite	YM_PORT_B,(BIT2)
	PsgWrite	YM_PORT_B,(BIT3)
	PsgWrite	YM_PORT_B,(BIT4)
	PsgWrite	YM_PORT_B,(BIT5)
	PsgWrite	YM_PORT_B,(BIT6)
	PsgWrite	YM_PORT_B,(BIT7)
	
	PsgWrite	YM_PORT_B,BITS_OFF
	
	PsgWrite	YM_PORT_B,(BIT7)
	PsgWrite	YM_PORT_B,(BIT6|BIT7)
	PsgWrite	YM_PORT_B,(BIT5|BIT6|BIT7)
	PsgWrite	YM_PORT_B,(BIT4|BIT5|BIT6|BIT7)
	PsgWrite	YM_PORT_B,(BIT3|BIT4|BIT5|BIT6|BIT7)
	PsgWrite	YM_PORT_B,(BIT2|BIT3|BIT4|BIT5|BIT6|BIT7)
	PsgWrite	YM_PORT_B,(BIT1|BIT2|BIT3|BIT4|BIT5|BIT6|BIT7)
	PsgWrite	YM_PORT_B,(BIT0|BIT1|BIT2|BIT3|BIT4|BIT5|BIT6|BIT7)
	PsgWrite	YM_PORT_B,(BIT1|BIT2|BIT3|BIT4|BIT5|BIT6|BIT7)
	PsgWrite	YM_PORT_B,(BIT2|BIT3|BIT4|BIT5|BIT6|BIT7)
	PsgWrite	YM_PORT_B,(BIT3|BIT4|BIT5|BIT6|BIT7)
	PsgWrite	YM_PORT_B,(BIT4|BIT5|BIT6|BIT7)
	PsgWrite	YM_PORT_B,(BIT5|BIT6|BIT7)
	PsgWrite	YM_PORT_B,(BIT6|BIT7)
	PsgWrite	YM_PORT_B,(BIT7)
	
	PsgWrite	YM_PORT_B,BITS_OFF

	SelectInHigh
	StrobeLow
	DataWriteMode

	bra.w  		mainloop
.quitRequest:
	; reset to initial state
	; /SelectIn low, /Busy high, /Strobe high
	lea.l  		PSG_BASE,a0 
	PsgWrite	YM_PORT_B,BITS_OFF
	SelectInLow
	StrobeHigh
	DataWriteMode
	
	jsr	super_off

	tos_cconin 	
			
	tos_halt

	include "common.asm"
; ##################################################################################################
; data section
    DATA

appinfo:
	if      BYPASS_XBIOS_CALLS
	dc.b 	"Atari Centronics test [using direct hardware access]",$0d,$0a
	else
	dc.b 	"Atari Centronics test [using XBIOS calls]",$0d,$0a
	endif

	dc.b 	"======================================",$0d,$0a
	dc.b 	"Pawel Goralski/[nokturnal]",$0d,$0a
	dc.b 	"(c)2022",$0d,$0a,0

byemsg:
	dc.b    $0d,$0a,$0d,$0a,"Press SPACE to return to desktop...",$0d,$0a,0
