; #################################################################################################
; # Atari ST/e/TT/F030 OPL2 / OPL3 Centronics / Cartridge scale test
; # Centronics/LPT port replay: Pawel Goralski / [nokturnal]
; # ROM port replay, os helper macros: Daniel Illgen / [TSCC]
; # (c) 2023 Pawel Goralski, Daniel Illgen
; #################################################################################################

LOOP_REPLAY				equ		1		; play tune in loop

; ##################################################################################################
; includes
	include "inc/tosmacros.inc"
	include "inc/common.inc"
	include "inc/opl.inc"

; ##################################################################################################
; code section
	TEXT

	if	USE_CENTRONICS_OUTPUT
		echo    'Building with output via Printer / Centronics port'
main:
	tos_init
	
	jsr		super_on
	ori.b 	#BIT0,LPT_DATA_DIRECTION.w		; output, bit #0 centronics busy

	StrobeHigh								; OPL2/3 /WR high

	if	ENABLE_OPL3	
	echo    'Building OPL3 test variant'
	SelectInLow
	else
	echo    'Building OPL2 test variant'
	SelectInLow
	endif

	tos_cconws appinfo	

	; reset opl
	jsr oplReset

	tos_cconws instrsetmsg

	lea.l 	oplRegs,a0  	
	lea.l 	oplRegsData,a1
	moveq 	#10,d6		;11 registers to write

.instrset:
	move.b  #0,d3		;set address
	move.b  (a0)+,d4
	jsr 	oplOut
	nop

	move.b  #OPL_DATA_WRITE,d3		;data write
	move.b  (a1)+,d4
	jsr 	oplOut
	nop
	
	dbra.w d6,.instrset

	tos_cconws playingscalemsg

scale:
	lea.l note_c,a4   	;set up first note address

.nextNote:
	move.b #OPL_ADDR_WRITE,d3		;set address
	move.b #$A0,d4
	jsr    oplOut
	nop 

	move.b  #OPL_DATA_WRITE,d3		;data write
	move.b  (a4)+,d4
	jsr 	oplOut
	nop

	move.b #OPL_ADDR_WRITE,d3		;set address
	move.b #$B0,d4
	jsr    oplOut
	nop

	move.b  #OPL_DATA_WRITE,d3		;data write
	move.b  (a4)+,d4
	ori.b   #%00110000,d4 			;add octave data & key on bit (note on)
	move.b  d4,-(sp)
	jsr 	oplOut
	nop

	move.w  #$FFFF,d0
.wait:
	subq.w  #1,d0
	cmpi.w  #$FFFF,d0

	move.b  (sp)+,d4
	andi.b  #%11011111,d4	;note off
	jsr 	oplOut

	lea.l   ACIA_DATA,a1
	cmpi.b  #SC_SPACE,(a1)
	beq    .quitRequest

	cmp.l  #note_b+2,a4
	bne    .nextNote

	bra.w  scale	

.quitRequest:
	
	; reset opl
	jsr oplReset

	; reset to initial state
	; /SelectIn low, /Busy high, /Strobe high 
	PsgWrite	YM_PORT_B,BITS_OFF
	SelectInLow
	StrobeHigh
	DataWriteMode
	
	jsr	super_off

	tos_cconws byemsg	
	tos_cconin 	
			
	tos_halt

	else
		echo    'Building with output via Cartridge port'

	tos_init
	jsr super_on

mainloop:
	;a0 psg adress
	;a1 acia data
	;a4 adlib data

	;lea		adlibdata,a4
	move.w	#200,d2
	
	lea.l  ACIA_DATA,a1

.readloop:
	cmpi.b  #SC_SPACE,(a1)
	beq    .quitRequest

	move.b	(a4)+,d3		; read register select
	beq		.adlibwait
	move.b	(a4)+,d4        ; read data

	bsr.w 	oplOut
	bra.s	.readloop
.adlibdone:
	rts

.adlibwait:
	;move.w	#$fff,$ffff8240.w
	move.l	$4ba.w,d3
.wt200:	
    cmp.l	$4ba.w,d3
	beq.s	.wt200
	sub.w	#70,d2
	bpl.s	.adlibwait
	add.w	#200,d2
	move.b	(a4)+,d3

	if      LOOP_REPLAY
	bmi.s	mainloop
	else	
	bmi.s	.adlibdone
	endif

	;move.w	#0,$ffff8240.w
	bra.s	.readloop

.quitRequest:

	jsr	super_off

	tos_cconws byemsg	
	tos_cconin 	
			
	tos_halt

	endif
	
	include "common.asm"
    include "opl.asm"
    
    DATA
	; instrument data
	oplRegs:
			dc.b $20,$40,$60,$80,$E0,$23,$43,$63,$83,$E3,$C0
	oplRegsData:	
			dc.b $01,$FF,$FF,$FF,$00,$01,$00,$F5,$75,$00,$3C

	; note data C,D,E,F,G,A,B - base frequency data only a440 pitch standard
	note_c: dc.b $59,$01
	note_d: dc.b $83,$01
	note_e: dc.b $B3,$01
	note_f: dc.b $CC,$01
	note_g: dc.b $05,$05
	note_a: dc.b $44,$02
	note_b: dc.b $8B,$02

appinfo:

	if ENABLE_OPL3

	if	USE_CENTRONICS_OUTPUT
	dc.b 	$0d,$0a,"OPL3LPT scale test",$0d,$0a
	dc.b 	"=========================================",$0d,$0a
	else
	dc.b 	$0d,$0a,"OPL3 Cartridge scale test",$0d,$0a
	dc.b 	"======================================",$0d,$0a
	endif
	
	else

	if	USE_CENTRONICS_OUTPUT
	dc.b 	$0d,$0a,"OPL2LPT scale test",$0d,$0a
	dc.b 	"=========================================",$0d,$0a
	else
	dc.b 	$0d,$0a,"OPL2 Cartridge port scale output test",$0d,$0a
	dc.b 	"======================================",$0d,$0a
	endif

	endif

	dc.b    "OPL2 Cartridge port replay, signal rewiring ideas:",$0d,$0a,"Daniel Illgen Insane/[tscc]",$0d,$0a
	dc.b 	"Centronics port replay, OPL3 support, adapters schematics:",$0d,$0a,"Pawel Goralski Saulot/[nokturnal]",$0d,$0a
	dc.b 	"(c)2023 [nokturnal]",$0d,$0a,0

instrsetmsg:
	dc.b    "* Setting instrument data.",$0d,$0a,0

playingscalemsg:
	dc.b    "* Now playing scale in a loop...",$0d,$0a,"Press SPACE to quit.",$0d,$0a,0

byemsg:
	dc.b    $0d,$0a,$0d,$0a,"Bye...",$0d,$0a,0