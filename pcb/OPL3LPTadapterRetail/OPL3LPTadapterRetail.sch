EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 7200 6900 0    168  ~ 0
Atari OPL3LPT adapter [retail]
Text Notes 3750 4750 1    207  ~ 0
OPL3LPT IN
Text Notes 7700 5750 1    207  ~ 0
Atari ST/STe/TT/F030
NoConn ~ 7850 4750
NoConn ~ 7850 3250
NoConn ~ 7850 2850
NoConn ~ 7850 2650
NoConn ~ 7850 4950
NoConn ~ 7850 4550
NoConn ~ 7850 4150
NoConn ~ 7850 3950
NoConn ~ 7850 3750
NoConn ~ 7850 3550
NoConn ~ 7850 3350
NoConn ~ 7850 3150
NoConn ~ 7850 2950
NoConn ~ 7850 2750
Wire Wire Line
	7950 4550 7850 4550
$Comp
L Switch:SW_SPDT SW1
U 1 1 5F08CFA4
P 3100 4350
F 0 "SW1" H 3100 4025 50  0000 C CNN
F 1 "OPL2/3 Switch" H 3100 4116 50  0000 C CNN
F 2 "Button_Switch_THT:SW_CuK_OS102011MA1QN1_SPDT_Angled" H 3100 4350 50  0001 C CNN
F 3 "~" H 3100 4350 50  0001 C CNN
	1    3100 4350
	-1   0    0    1   
$EndComp
Wire Wire Line
	8150 5250 8150 6200
$Comp
L Connector:DB25_Male_MountingHoles J2
U 1 1 5F014C4F
P 8150 3850
F 0 "J2" H 8330 3852 50  0000 L CNN
F 1 "DB25_Male_MountingHoles" H 8330 3761 50  0000 L CNN
F 2 "Connector_Dsub:DSUB-25_Male_Horizontal_P2.77x2.84mm_EdgePinOffset4.94mm_Housed_MountingHolesOffset7.48mm" H 8150 3850 50  0001 C CNN
F 3 " ~" H 8150 3850 50  0001 C CNN
	1    8150 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 5050 7850 6000
Wire Wire Line
	7800 5500 7800 4850
Wire Wire Line
	7800 4850 7850 4850
Wire Wire Line
	7750 4650 7850 4650
Wire Wire Line
	7750 4650 7750 5000
Wire Wire Line
	7650 4550 7650 4450
Wire Wire Line
	7650 4450 7850 4450
Wire Wire Line
	6750 4100 6750 4250
Wire Wire Line
	6750 4250 7850 4250
$Comp
L Connector:DB25_Female_MountingHoles J1
U 1 1 5F00D720
P 2150 3850
F 0 "J1" H 2329 3759 50  0000 L CNN
F 1 "DB25_Female_MountingHoles" H 2329 3850 50  0000 L CNN
F 2 "Connector_Dsub:DSUB-25_Female_Horizontal_P2.77x2.84mm_EdgePinOffset4.94mm_Housed_MountingHolesOffset7.48mm" H 2150 3850 50  0001 C CNN
F 3 " ~" H 2150 3850 50  0001 C CNN
	1    2150 3850
	-1   0    0    1   
$EndComp
Wire Wire Line
	2450 6000 2450 5050
Wire Wire Line
	2450 4850 2550 4850
Wire Wire Line
	2550 4850 2550 5500
Wire Wire Line
	2600 5000 2600 4650
Wire Wire Line
	2600 4650 2450 4650
Wire Wire Line
	2600 4550 2600 4450
Wire Wire Line
	2600 4450 2450 4450
Wire Wire Line
	2600 4100 2600 4250
Wire Wire Line
	2600 4250 2450 4250
Wire Wire Line
	2900 4450 2900 4350
Wire Wire Line
	2900 4350 2450 4350
Wire Wire Line
	2450 4550 2450 4600
NoConn ~ 2450 3250
NoConn ~ 2450 3050
NoConn ~ 2450 2850
NoConn ~ 2450 2650
NoConn ~ 2450 4750
NoConn ~ 2450 3950
NoConn ~ 2450 3750
NoConn ~ 2450 3550
NoConn ~ 2450 3350
Wire Wire Line
	8150 6200 3900 6200
Wire Wire Line
	3900 6200 3900 4200
Wire Wire Line
	3900 2450 2150 2450
Wire Wire Line
	2150 2150 2150 2450
Connection ~ 2150 2450
Wire Wire Line
	2450 4950 4700 4950
Wire Wire Line
	4700 4950 4700 2150
NoConn ~ 2450 2950
NoConn ~ 2450 2750
NoConn ~ 2450 3150
Text Notes 8150 7650 0    89   ~ 0
04.07.2020
Text Notes 10600 7650 0    89   ~ 0
A
Text Notes 7350 7500 0    89   ~ 18
Atari ST/TT/F030 to OPL3PLT adapter retail
Text Label 6900 6200 0    89   ~ 0
GND
Text Label 6900 6000 0    89   ~ 0
STROBE
Text Label 6900 5500 0    89   ~ 0
DATA0
Text Label 6900 5000 0    89   ~ 0
DATA1
Text Label 6900 4550 0    89   ~ 0
DATA2
Text Label 6900 4250 0    89   ~ 0
DATA3
Text Label 6900 4050 0    89   ~ 0
DATA4
Text Label 6900 3850 0    89   ~ 0
DATA5
Text Label 6900 3650 0    89   ~ 0
DATA6
Text Label 6900 3450 0    89   ~ 0
DATA7
Text Label 6950 3050 0    89   ~ 0
BUSY
Text Label 4800 4350 0    89   ~ 0
SELECTIN
NoConn ~ 2450 4150
Wire Wire Line
	2900 4250 2900 4200
Wire Wire Line
	2900 4200 3900 4200
Connection ~ 3900 4200
Wire Wire Line
	3900 4200 3900 2450
Text Label 2650 6000 0    89   ~ 0
A0
Text Label 2650 5500 0    89   ~ 0
D0
Text Label 2650 5000 0    89   ~ 0
D1
Text Label 4150 4950 0    89   ~ 0
RD
Text Label 2650 4550 0    89   ~ 0
D2
Text Label 2600 4250 0    89   ~ 0
D3
Text Label 2600 4050 0    89   ~ 0
D4
Text Label 2600 3850 0    89   ~ 0
D5
Text Label 2600 3650 0    89   ~ 0
D6
Text Label 2600 3450 0    89   ~ 0
D7
Wire Wire Line
	2450 6000 7850 6000
Wire Wire Line
	2550 5500 7800 5500
Wire Wire Line
	2600 5000 7750 5000
Wire Wire Line
	2600 4550 7650 4550
Wire Wire Line
	3300 4350 7850 4350
Wire Wire Line
	2600 4100 6750 4100
Wire Wire Line
	4700 2150 2150 2150
Wire Wire Line
	2450 4050 7850 4050
Wire Wire Line
	2450 3850 7850 3850
Wire Wire Line
	2450 3650 7850 3650
Wire Wire Line
	2450 3450 7850 3450
Wire Wire Line
	6500 3050 6500 4600
Wire Wire Line
	6500 4600 2450 4600
Wire Wire Line
	6500 3050 7850 3050
$EndSCHEMATC
