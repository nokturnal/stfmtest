; #################################################################################################
; # Atari ST/e/TT/F030 OPL2 / OPL3 chip reset
; # Centronics/LPT port replay: Pawel Goralski / [nokturnal]
; # (c) 2023 Pawel Goralski, Daniel Illgen
; #################################################################################################

; ##################################################################################################
; includes
	include "inc/tosmacros.inc"
	include "inc/common.inc"
	include "inc/opl.inc"
	
; ##################################################################################################
; code section
	TEXT

	if	USE_CENTRONICS_OUTPUT
		echo    'Building with output via Printer / Centronics port'
main:
	tos_init
	
	jsr		super_on
	ori.b 	#BIT0,LPT_DATA_DIRECTION.w		; output, bit #0 centronics busy

	StrobeHigh								; OPL2/3 /WR high

	if	ENABLE_OPL3	
	echo    'Building OPL3 test variant'
	SelectInLow
	else
	echo    'Building OPL2 test variant'
	SelectInLow
	endif

	jsr oplReset

	; reset to initial state
	; /SelectIn low, /Busy high, /Strobe high 
	PsgWrite	YM_PORT_B,BITS_OFF
	SelectInLow
	StrobeHigh
	DataWriteMode
	
	jsr	super_off

	tos_halt

	else
		echo    'Building with output via Cartridge port'

	tos_init
	jsr super_on

	jsr oplReset

	jsr	super_off
			
	tos_halt

	endif

	include "common.asm"
	include "opl.asm"