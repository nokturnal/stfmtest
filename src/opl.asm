; #################################################################################################
; # Atari ST/e/TT/F030 OPL2 / OPL3 helper functions
; # Centronics/LPT port replay: Pawel Goralski / [nokturnal]
; # ROM port replay, os helper macros: Daniel Illgen / [TSCC]
; # (c) 2023 Pawel Goralski, Daniel Illgen
; #################################################################################################

	TEXT
; ------------------------------------------------------------------------------------------------
; function: oplOut
; writes to opl2/3 chip
; d2 = OPL_REGISTERSET0 - OPL2 register set, OPL_REGISTERSET1 - OPL3 register set
; d3 = opl2/3 register access mode (OPL_ADDR_WRITE - address write mode, OPL_DATA_WRITE - data write mode)
; d4 = data to write

; on enter strobe is set to high (\BUSY high),
; clean up d3,d4
; if d3 == 0 then register select, 
; else set register select and perform lpt data write, delay 4us. 
; if d4 == 1 then data register write, set register data write, perform lpt data write, delay 24us  
; Set Strobe low (/BUSY), wait  100ns, set Strobe High (/BUSY)
; finish

oplOut:
	movem.l d0-6,-(sp)
	
	if	USE_CENTRONICS_OUTPUT
	AddressWriteMode
	
	if ENABLE_OPL3
	cmpi.b 	#OPL_REGISTERSET0,d2
	beq.s   .setOpl2Mode
	SelectInHigh
	bra.s .modeIsSet
.setOpl2Mode:
	SelectInLow
.modeIsSet:
	else
	SelectInLow
	endif

	PsgWriteReg	YM_PORT_B,d3

	StrobeLow			; /WR low
                        ; delay after /WR low
	rept OPL_DELAY_STROBE_HOLD
	nop 
	endr
	
	StrobeHigh			; OPL2/3 /WR high

	; delay after address write
	rept OPL_DELAY_ADDR_WRITE
	nop 
	endr

	DataWriteMode
	PsgWriteReg	YM_PORT_B,d4

	StrobeLow			; /WR low
						; delay after /WR low
	rept OPL_DELAY_STROBE_HOLD
	nop 
	endr

	StrobeHigh			; OPL2/3 /WR high
	
	; register data write delay
	rept OPL_DELAY_DATA_WRITE
	nop 
	endr

	else
	
	; centronics part
	; The 'tst' lines put the address lines on the cart connector - we can't write on the cart port, so we need to use reads "for writes"
	; as well as using the address lines as data lines. 
	; reading from fa0000 selects reg-select/opl2-a0=0; reading from fa0200 select regs-data/opl2-a0=1.
	; a2 is only used for timing purposes.

	move.l	#$fa0000,a0 	; reading from fa0000 selects reg-select/opl2-a0=0; 
	move.l	#$fa0200,a1		; reading from fa0200 select regs-data/opl2-a0=1.
	move.l	#$fb0000,a2		; a2 - timing
	and.w	#$ff,d3
	and.w	#$ff,d4
	add.w	d3,d3
	add.w	d4,d4
	tst.b	(a0,d3)
;	tst.b	(a0,d0.w*2)
;   tst.w (a0)=8
;   tst.w (0(a0,d0.w))=16
;   same as move

	rept OPL_DELAY_ADDR_WRITE
	nop 
	endr

;	or.w	#$0100,d1
;	tst.b	(a0,d1.w*2)
	tst.w	(a1,d4)

	rept OPL_DELAY_DATA_WRITE
	nop 
	endr
	
	endif

	movem.l (sp)+,d0-6
	rts

resetOplRegisterRange:
	move.b d0,d2 				; store opl2 / 3 mode
	
	move.b (a0)+,d0             ; register number start range
	clr.l  d6
	move.b (a0)+,d6             ; register number end range
	
	sub.b  d0,d6                ; d6 = end range - start range
	subq   #1,d6				; d6 -= 1

.submit:
								; d2 contains opl2/3 mode
	move.b #OPL_ADDR_WRITE,d3	; set address mode
	move.b d0,d4                ;
	jsr    oplOut
	nop 

	; d2 already set earlier
	move.b  #OPL_DATA_WRITE,d3	; set data write mode
	clr.l   d4             		; clear
	jsr 	oplOut
	nop	

	addq.b #1,d0 				; increase opl2 register number
	dbra   d6,.submit           ; next value

	rts

oplReset:
	tos_cconws oplresetmsg

	; ########################### reset opl
	; reset opl2 / 3 register set 0

	lea.l 	treVibResetData,a0
	move.b 	#OPL_REGISTERSET0,d0
	jsr 	resetOplRegisterRange
	
	lea.l 	freqResetData,a0
	move.b  #OPL_REGISTERSET0,d0
	jsr 	resetOplRegisterRange
	
	lea.l 	keyOffResetData,a0
	move.b  #OPL_REGISTERSET0,d0
	jsr 	resetOplRegisterRange

	lea.l 	feedbackResetData,a0
	move.b  #OPL_REGISTERSET0,d0
	jsr 	resetOplRegisterRange

	lea.l 	keyOffResetData,a0
	move.b  #OPL_REGISTERSET0,d0
	jsr 	resetOplRegisterRange

	; drum off reset
	move.b  #OPL_REGISTERSET0,d2
	move.b  #OPL_ADDR_WRITE,d3		
	move.b  #$bd,d4
	jsr 	oplOut
	nop

	move.b  #OPL_REGISTERSET0,d2
	move.b  #OPL_DATA_WRITE,d3
	move.b  #0,d4
	jsr 	oplOut
	nop

	; CSW / NoteSel reset
	move.b  #OPL_REGISTERSET0,d2
	move.b  #OPL_ADDR_WRITE,d3		
	move.b  #$08,d4
	jsr 	oplOut
	nop

	move.b  #OPL_REGISTERSET0,d2
	move.b  #OPL_DATA_WRITE,d3
	move.b  #0,d4
	jsr 	oplOut
	nop	

	; wave reset 
	move.b  #OPL_REGISTERSET0,d2
	move.b  #OPL_ADDR_WRITE,d3		
	move.b  #$01,d4
	jsr 	oplOut
	nop

	move.b  #OPL_REGISTERSET0,d2
	move.b  #OPL_DATA_WRITE,d3
	move.b  #0,d4
	jsr 	oplOut
	nop	
	
	if	ENABLE_OPL3	

	; reset OPL3 register set 1
	
	lea.l 	treVibResetData,a0
	move.b  #OPL_REGISTERSET1,d0
	jsr 	resetOplRegisterRange
	
	lea.l 	freqResetData,a0
	move.b  #OPL_REGISTERSET1,d0
	jsr 	resetOplRegisterRange
	
	lea.l 	keyOffResetData,a0
	move.b  #OPL_REGISTERSET1,d0
	jsr 	resetOplRegisterRange

	lea.l 	feedbackResetData,a0
	move.b  #OPL_REGISTERSET1,d0
	jsr 	resetOplRegisterRange

	lea.l 	keyOffResetData,a0
	move.b  #OPL_REGISTERSET1,d0
	jsr 	resetOplRegisterRange

	; drum off reset
	move.b  #OPL_REGISTERSET1,d2
	move.b  #OPL_ADDR_WRITE,d3		
	move.b  #$bd,d4
	jsr 	oplOut
	nop

	move.b  #OPL_REGISTERSET1,d2
	move.b  #OPL_DATA_WRITE,d3
	move.b  #0,d4
	jsr 	oplOut
	nop

	; CSW / NoteSel reset
	move.b  #OPL_REGISTERSET1,d2
	move.b  #OPL_ADDR_WRITE,d3		
	move.b  #$08,d4
	jsr 	oplOut
	nop

	move.b  #OPL_REGISTERSET1,d2
	move.b  #OPL_DATA_WRITE,d3
	move.b  #0,d4
	jsr 	oplOut
	nop	

	; wave reset 
	move.b  #OPL_ADDR_WRITE,d3		
	move.b  #$01,d4
	jsr 	oplOut
	nop

	move.b  #OPL_DATA_WRITE,d3
	move.b  #0,d4
	jsr 	oplOut
	nop	

	; opl3 only 
	;  4-op enable
	move.b  #OPL_ADDR_WRITE,d3		
	move.b  #$04,d4
	jsr 	oplOut
	nop

	move.b  #OPL_DATA_WRITE,d3
	move.b  #0,d4
	jsr 	oplOut
	nop	

	; set OPL2 compatibility mode
	move.b  #OPL_ADDR_WRITE,d3		
	move.b  #$05,d4
	jsr 	oplOut
	nop

	move.b  #OPL_DATA_WRITE,d3
	move.b  #0,d4
	jsr 	oplOut
	nop	
	
	SelectInLow
	endif
	rts

	DATA
oplresetmsg:
	dc.b    $0d,$0a,"* OPL2/3 reset.",$0d,$0a,0

treVibResetData:
	dc.b $20,$35

freqResetData:
	dc.b $a0,$a8
	
keyOffResetData:
	dc.b $b0,$b8
	
feedbackResetData:
	dc.b $c0,$c8

waveformResetData:
	dc.b $e0,$f5
