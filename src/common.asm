; #################################################################################################
; # Atari ST/e/TT/F030 Common functions
; # (c) 2019-2022 Pawel Goralski / [nokturnal]
; #################################################################################################

	TEXT
	; enter supervisor mode
super_on:
	movem.l	d0-7/a0-a6,-(sp)

	clr.l	-(sp)
	move.w	#$20,-(sp)
	trap	#1
	addq.l	#6,sp
	move.l	d0,old_ssp
.skip:
	movem.l	(sp)+,d0-7/a0-a6
	RTS

; leave supervisor mode
super_off:
	movem.l	d0-7/a0-a6,-(sp)
	move.l	old_ssp,-(sp)
	move.w	#$20,-(sp)
	trap	#1
	addq.l	#6,sp
	movem.l	(sp)+,d0-7/a0-a6 
	RTS

	BSS
old_ssp:	ds.l 1

	TEXT
