; #################################################################################################
; # Atari ST/e/TT/F030 OPL2/3 defines and helper functions
; # (c) 2019-2023 Pawel Goralski / [nokturnal]
; #################################################################################################
	
ENABLE_16MHZ_CLOCK		equ		1		; 1 - falcon030 / mste, 0 - plain ST/STe 
BYPASS_XBIOS_CALLS		equ		1		; write directly to PSG and MFP registers 
BYPASS_XBIOS_INTOFF		equ 	0       ; enable turning off interrupts on PSG register access like in XBIOS calls

	if ENABLE_OPL3
			echo    'Using delays for OPL3'

; OPL3 timings
; Master clock = 14,32MHz
; minimal time after address write mode = 32 cycles 
; minimal time after data write mode = 32 cycles

; 32 / 14,32 = ~ 2,23µs after the address write if we write data
; 32 / 14,32 = ~ 2,23µs after the data write

; 100ns strobe hold

	if ENABLE_16MHZ_CLOCK
		echo    '16mhz clock timings [F030 / MSTE]'
OPL_DELAY_ADDR_WRITE	equ		4
OPL_DELAY_DATA_WRITE	equ		4
OPL_DELAY_STROBE_HOLD	equ		1
    else
		echo    '8mhz clock timings [ST / STe]'
OPL_DELAY_ADDR_WRITE	equ		0
OPL_DELAY_DATA_WRITE	equ		0
OPL_DELAY_STROBE_HOLD	equ		0
    endif

    else
    	echo    'Using delays for OPL2'

; OPL2 timings
; Master clock = 3,58 Mhz
; minimal time after address write mode = 12 cycles 
; minimal time after data write mode = 84 cycles

; 12 / 3,58 = ~ 3,3 µs after the address write
; 84 / 3,58 = ~ 23,5 µs after the data write

; 100ns strobe hold

	if ENABLE_16MHZ_CLOCK
		echo    '16mhz clock timings [F030 / MSTE]'
OPL_DELAY_ADDR_WRITE	equ		4
OPL_DELAY_DATA_WRITE	equ		122
OPL_DELAY_STROBE_HOLD	equ		4
    else
		echo    '8mhz clock timings [ST / STe]'
OPL_DELAY_ADDR_WRITE	equ		0
OPL_DELAY_DATA_WRITE	equ		40
OPL_DELAY_STROBE_HOLD	equ		0
    endif

    endif	

; ##################################################################################################
; enums

YM_PORT_A				equ		14
YM_PORT_B				equ		15

PSG_BASE				equ		$ffff8800
LPT_PORT_DATA_REG		equ		$fffffa01
LPT_DATA_DIRECTION		equ		$fffffa05

BIT0					equ 	1<<0
BIT1					equ 	1<<1
BIT2					equ 	1<<2
BIT3					equ 	1<<3
BIT4					equ 	1<<4
BIT5					equ 	1<<5
BIT6					equ 	1<<6
BIT7					equ 	1<<7

; aliases
STROBE_BIT				equ		BIT5  
SELECTIN_BIT			equ		BIT3

BITS_OFF				equ		~(BIT0|BIT1|BIT2|BIT3|BIT4|BIT5|BIT6|BIT7)
BITS_ON					equ		(BIT0|BIT1|BIT2|BIT3|BIT4|BIT5|BIT6|BIT7)

; #####################################################################################
; macros

	if 		BYPASS_XBIOS_CALLS
	echo    'Building with Centronics functions bypassing XBIOS system calls'
	else
	echo    'Building with Centronics functions using XBIOS system calls'
	endif

PsgRead	macro ymRegisterNb
	if 		BYPASS_XBIOS_CALLS

	if      BYPASS_XBIOS_INTOFF
	move.w  sr,-(sp)
	ori.w   #$0700,sr
	endif

	move.b  #\1,(a0)
	move.b  (a0),d0
	
	if      BYPASS_XBIOS_INTOFF
	move.w	(sp)+,sr
	endif

	else

	move.w 	#\1,d0
	andi.w  #$000F,d0  			; read mode
	move.w 	d0,-(sp)
	move.w 	#0,-(sp)
	move.w  #28,-(sp)			; giaccess
	trap 	#14
	addq.l 	#6,sp

	endif
	endm

PsgWrite	macro ymRegisterNb,newState
	if 		BYPASS_XBIOS_CALLS

	if      BYPASS_XBIOS_INTOFF
	move.w  sr,-(sp)
	ori.w   #$0700,sr
	endif

	move.b 	#\1,(a0)			; select PSG register (0-15)					
	move.w  #\2,d0
	move.b  d0,2(a0)			; write data $FF8802

	if      BYPASS_XBIOS_INTOFF
	move.w	(sp)+,sr
	endif

	else

	move.w 	#\1,d1           	; register nb (0-15)
	ori.w   #$0080,d1  			; write mode
	move.w 	d1,-(sp)
	move.w  #\2,d1
	and.w   #$00ff,d1
	move.w 	d1,-(sp)
	move.w  #28,-(sp) 			; giaccess
	trap 	#14
	addq.l 	#6,sp
	endif
	endm

PsgWriteReg	macro ymRegisterNb,dataReg   
	if 		BYPASS_XBIOS_CALLS

	if      BYPASS_XBIOS_INTOFF
	move.w  sr,-(sp)
	ori.w   #$0700,sr
	endif

	move.b 	#\1,(a0)			; select PSG register					
	move.b  \2,2(a0) 					
	
	if      BYPASS_XBIOS_INTOFF
	move.w	(sp)+,sr
	endif

	else

	move.w 	#\1,d7
	andi.w  #$000F,d7
	ori.b   #$80,d7  			; write mode
	move.w 	d7,-(sp)
	andi.w 	#$00ff,\2
	move.w 	\2,-(sp)
	move.w  #28,-(sp)			; giaccess
	trap 	#14
	addq.l 	#6,sp

	endif
	endm

; OPL2/3 LPT/Parallel port control
DataWriteMode macro                         ; A0 [L]ow
	lea.l  			PSG_BASE,a0
	PsgRead			YM_PORT_A				; read Port A state
	if 		BYPASS_XBIOS_CALLS
	ori.b 			#STROBE_BIT,d0			; reset bit #5 (Strobe High, 0 - High, Inverted bits) 
	else
	ori.b 			#STROBE_BIT,d0			; reset bit #5 (Strobe High, 0 - High, Inverted bits) 
	endif

	PsgWriteReg		YM_PORT_A,d0 			; write new port A state
	endm

AddressWriteMode macro                      ; A0 [H]igh
	lea.l  			PSG_BASE,a0             
	PsgRead			YM_PORT_A				; read Port A state

	if 		BYPASS_XBIOS_CALLS
	andi.b 			#(~STROBE_BIT),d0		; set bit #5 (Strobe Low, 1 - Low) Inverted bits
	else
	andi.b 			#(~STROBE_BIT),d0		; set bit #5 (Strobe Low, 1 - Low) Inverted bits
	endif

	PsgWriteReg		YM_PORT_A,d0 			; write new port A state
	endm

SelectInHigh macro                          ; A1 [H]igh
	lea.l  			PSG_BASE,a0
	PsgRead			YM_PORT_A				; read Port A state
	ori.b 			#SELECTIN_BIT,d0		; set bit #4 (Strobe Low, 1 - Low) Inverted bits
	PsgWriteReg		YM_PORT_A,d0 			; write new port A state
	endm

SelectInLow macro                           ; A1 [L]ow
	lea.l  			PSG_BASE,a0
	PsgRead			YM_PORT_A				; read Port A state
	andi.b  		#~(SELECTIN_BIT),d0		; reset bit #4  (Strobe High, 0 - High, Inverted bits)
	PsgWriteReg		YM_PORT_A,d0 			; write new port A state
	endm

StrobeHigh macro                            ; /WR line [L]ow
	;andi.b 	#(~BIT0),LPT_PORT_DATA_REG.w	; /BUSY, (Strobe High, 0 - High, Inverted bits)
	ori.b 	#BIT0,LPT_PORT_DATA_REG.w
	endm

StrobeLow macro                             ; /WR line [H]igh  
	;ori.b 	#BIT0,LPT_PORT_DATA_REG.w		; /BUSY, (Strobe Low, 1 - Low) Inverted bits
	andi.b 	#(~BIT0),LPT_PORT_DATA_REG.w
	endm

; ############ defines
OPL_ADDR_WRITE          equ     $00
OPL_DATA_WRITE          equ     $01
OPL_REGISTERSET0 		equ 	$00
OPL_REGISTERSET1 		equ 	$01

; ------------------------------------------------------------------------------------------------
; function: oplOut
; writes to opl2/3 chip
; d2 = OPL_REGISTERSET0 - OPL2 register set, OPL_REGISTERSET1 - OPL3 register set
; d3 = opl2/3 register access mode (OPL_ADDR_WRITE - address write mode, OPL_DATA_WRITE - data write mode)
; d4 = data to write

	XDEF	oplOut

; ------------------------------------------------------------------------------------------------
; function: oplReset
; resets opl2/3 chip

	XDEF	oplReset
